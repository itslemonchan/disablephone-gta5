﻿using System;
using GTA;
using GTA.Native;

namespace DisablePhone
{
	public class DisablePhone : Script
	{
		public DisablePhone()
		{
			Interval = 0;
			Tick += DisablePhone_Tick;
		}

		private void DisablePhone_Tick(object sender, EventArgs e)
		{
			Function.Call(Hash.TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME, "cellphone_controller");
			Game.DisableControlThisFrame(0, Control.Phone);
		}
	}
}